# Harpy Service

The Harpy Service provides an API that enables users to work with FHIR resources. It includes an underlying Blaze FHIR server (see [docker-compose.yml](docker-compose.yml)).

The following packages (amongst others) are included in this service:

* `fhir-py` to handle FHIR resources asynchronously/synchronously
* `fastapi` to define HTTP/REST routes
* `typer` to setup the CLI command `tsctl` (Harpy Service Control), providing develop tools (see `tsctl --help`)

## Dev Setup

* install docker
* install docker compose
* install poetry
* clone harpy-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd harpy-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker compose`.

```bash
docker compose up --build -d
```

Or start `harpy_service` with uvicorn and only a development database with docker compose.

```bash
docker compose up -d harpy-service-db
uvicorn --host=0.0.0.0 --port=8000 --reload harpy_service.app:app
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs
* http://localhost:8000/v3/docs

### Stop and Remove

```bash
docker compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle harpy_service tests
pylint harpy_service tests
pytest --maxfail=1  # requires service running
```
