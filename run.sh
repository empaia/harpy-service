#!/usr/bin/env bash

hsctl migrate-db && uvicorn harpy_service.app:app $@
