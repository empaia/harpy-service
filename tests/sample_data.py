SAMPLE_QUESTIONNAIRE = {
    "resourceType": "Questionnaire",
    "name": "ASLPA1",
    "title": "ASLP.A1 Adult Sleep Studies",
    "status": "active",
    "description": "Adult Sleep Studies Prior Authorization Form",
    "useContext": [
        {
            "code": {
                "system": "http://terminology.hl7.org/CodeSystem/usage-context-type",
                "code": "task",
                "display": "Workflow Task",
            },
            "valueCodeableConcept": {
                "coding": [
                    {
                        "system": "http://fhir.org/guides/nachc/hiv-cds/CodeSystem/activity-codes",
                        "code": "ASLP.A1",
                        "display": "Adult Sleep Studies",
                    }
                ]
            },
        }
    ],
    "item": [
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "type": "group",
            "repeats": True,
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "type": "choice",
                    "answerValueSet": "http://example.org/sdh/dtr/aslp/ValueSet/aslp-a1-de1-codes-grouper",
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "type": "dateTime",
                },
            ],
        },
        {
            "linkId": "3",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-diagnosis-of-obstructive-sleep-apnea#Condition.code",
            "text": "Diagnosis of Obstructive Sleep Apnea",
            "type": "choice",
            "answerValueSet": "http://example.org/sdh/dtr/aslp/ValueSet/aslp-a1-de17",
        },
        {
            "linkId": "4",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-hypertension#Observation.value[x]",
            "text": "History of Hypertension",
            "type": "boolean",
        },
        {
            "linkId": "5",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-diabetes#Observation.value[x]",
            "text": "History of Diabetes",
            "type": "boolean",
        },
        {
            "linkId": "6",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Neck circumference (in inches)",
            "type": "quantity",
        },
        {
            "linkId": "7",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Height (in inches)",
            "type": "quantity",
        },
        {
            "linkId": "8",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-weight#Observation.value[x]",
            "text": "Weight (in pounds)",
            "type": "quantity",
        },
        {
            "linkId": "9",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-bmi#Observation.value[x]",
            "text": "Body mass index (BMI)",
            "type": "quantity",
        },
        {
            "linkId": "10",
            "text": "Additional Information?",
            "type": "choice",
            "answerConstraint": "optionsOrString",
            "answerOption": [{"valueString": "Yes (specify)"}, {"valueString": "No"}],
        },
    ],
}


SAMPLE_QUESTIONNAIRE_RESPONSE = {
    "resourceType": "QuestionnaireResponse",
    "questionnaire": "Questionnaire/1",
    "status": "in-progress",
    "item": [
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "answer": [
                        {
                            "valueCoding": {
                                "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                                "code": "ASLP.A1.DE2",
                                "display": "Home sleep apnea testing (HSAT)",
                            }
                        }
                    ],
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "answer": [{"valueDateTime": "2023-04-10T08:00:00.000Z"}],
                },
            ],
        },
        {
            "linkId": "0",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-sleep-study-order",
            "text": "A sleep study procedure being ordered",
            "item": [
                {
                    "linkId": "1",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.code",
                    "text": "A sleep study procedure being ordered",
                    "answer": [
                        {
                            "valueCoding": {
                                "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                                "code": "ASLP.A1.DE14",
                                "display": "Artificial intelligence (AI)",
                            }
                        }
                    ],
                },
                {
                    "linkId": "2",
                    "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
                    "aslp-sleep-study-order#ServiceRequest.occurrence[x]",
                    "text": "Date of the procedure",
                    "answer": [{"valueDateTime": "2023-04-15T08:00:00.000Z"}],
                },
            ],
        },
        {
            "linkId": "3",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-diagnosis-of-obstructive-sleep-apnea#Condition.code",
            "text": "Diagnosis of Obstructive Sleep Apnea",
            "answer": [
                {
                    "valueCoding": {
                        "system": "http://example.org/sdh/dtr/aslp/CodeSystem/aslp-codes",
                        "code": "ASLP.A1.DE17",
                        "display": "Obstructive sleep apnea (OSA)",
                    }
                }
            ],
        },
        {
            "linkId": "4",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-hypertension#Observation.value[x]",
            "text": "History of Hypertension",
            "answer": [{"valueBoolean": True}],
        },
        {
            "linkId": "5",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/"
            "aslp-history-of-diabetes#Observation.value[x]",
            "text": "History of Diabetes",
            "answer": [{"valueBoolean": True}],
        },
        {
            "linkId": "6",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Neck circumference (in inches)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 16,
                        "unit": "[in_i]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[in_i]",
                    }
                }
            ],
        },
        {
            "linkId": "7",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-height#Observation.value[x]",
            "text": "Height (in inches)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 69,
                        "unit": "[in_i]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[in_i]",
                    }
                }
            ],
        },
        {
            "linkId": "8",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-weight#Observation.value[x]",
            "text": "Weight (in pounds)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 185,
                        "unit": "[lb_av]",
                        "system": "http://unitsofmeasure.org",
                        "code": "[lb_av]",
                    }
                }
            ],
        },
        {
            "linkId": "9",
            "definition": "http://example.org/sdh/dtr/aslp/StructureDefinition/aslp-bmi#Observation.value[x]",
            "text": "Body mass index (BMI)",
            "answer": [
                {
                    "valueQuantity": {
                        "value": 16.2,
                        "unit": "kg/m2",
                        "system": "http://unitsofmeasure.org",
                        "code": "kg/m2",
                    }
                }
            ],
        },
        {"linkId": "10", "text": "Additional Information?", "answer": [{"valueString": "No"}]},
    ],
}
