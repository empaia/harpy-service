import requests

from tests.sample_data import SAMPLE_QUESTIONNAIRE

from .singletons import hs_url


def test_post_questionnaire():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp1 = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}")
    r.raise_for_status()
    resp2 = r.json()

    assert resp1 == resp2


def test_alter_questionnaire():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp1 = r.json()

    altered_questionnaire = SAMPLE_QUESTIONNAIRE.copy()
    altered_questionnaire["title"] = "Altered Questionnaire"
    del altered_questionnaire["useContext"]
    del altered_questionnaire["item"][0]

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}", json=altered_questionnaire)
    r.raise_for_status()
    resp2 = r.json()

    assert resp1 != resp2
    assert resp2["title"] == "Altered Questionnaire"
    assert resp2["useContext"] is None
    assert len(resp2["item"]) == len(resp1["item"]) - 1
    assert int(resp2["meta"]["versionId"]) == int(resp1["meta"]["versionId"]) + 1


def test_questionnaire_history():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp1 = r.json()

    altered_questionnaire1 = SAMPLE_QUESTIONNAIRE.copy()
    altered_questionnaire1["title"] = "TITLE 1"
    altered_questionnaire2 = SAMPLE_QUESTIONNAIRE.copy()
    altered_questionnaire2["title"] = "TITLE 2"

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}", json=altered_questionnaire1)
    r.raise_for_status()
    resp2 = r.json()

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}", json=altered_questionnaire2)
    r.raise_for_status()
    resp3 = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}/history")
    r.raise_for_status()
    resp_history = r.json()

    assert len(resp_history) == 3
    assert resp_history[0] == resp3
    assert resp_history[1] == resp2
    assert resp_history[2] == resp1


def test_questionnaire_history_item():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp1 = r.json()

    altered_questionnaire1 = SAMPLE_QUESTIONNAIRE.copy()
    altered_questionnaire1["title"] = "TITLE 1"
    altered_questionnaire2 = SAMPLE_QUESTIONNAIRE.copy()
    altered_questionnaire2["title"] = "TITLE 2"

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}", json=altered_questionnaire1)
    r.raise_for_status()
    _resp2 = r.json()

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}", json=altered_questionnaire2)
    r.raise_for_status()
    resp3 = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/questionnaires/{resp1['id']}/history/{resp1['meta']['versionId']}")
    r.raise_for_status()
    resp_history_item = r.json()

    assert resp_history_item == resp1

    r = requests.get(f"{hs_url}/v3/fhir/questionnaires/{resp3['id']}/history/{resp3['meta']['versionId']}")
    r.raise_for_status()
    resp_history_item = r.json()

    assert resp_history_item == resp3


def test_query_questionnaire():
    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query", json={})
    r.raise_for_status()
    resp_query1 = r.json()

    # when tests are run multiple time, number of items can exceed query items limit
    assert resp_query1["item_count"] >= len(resp_query1["items"])

    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp1 = r.json()

    another_questionnaire = SAMPLE_QUESTIONNAIRE.copy()
    another_questionnaire["status"] = "draft"

    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=another_questionnaire)
    r.raise_for_status()
    resp2 = r.json()

    query = {}

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query", json=query)
    r.raise_for_status()
    resp_query2 = r.json()

    assert resp_query1["item_count"] + 2 == resp_query2["item_count"]
    assert len(resp_query1["items"]) + 2 >= len(resp_query2["items"])

    query["questionnaires"] = [resp1["id"]]

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query", json=query)
    r.raise_for_status()
    resp_query3 = r.json()

    assert resp_query3["item_count"] == len(resp_query3["items"]) == 1
    assert resp_query3["items"][0]["id"] == resp1["id"]

    query = {"questionnaires": [resp1["id"], resp2["id"]], "statuses": ["draft"]}

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query", json=query)
    r.raise_for_status()
    resp_query4 = r.json()

    assert resp_query4["item_count"] == len(resp_query3["items"]) == 1
    assert resp_query4["items"][0]["id"] == resp2["id"]


def test_query_questionnaire_exceeding_url_limit():
    questionnaire_ids = []
    for _i in range(0, 1000):
        r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
        r.raise_for_status()
        resp1 = r.json()
        questionnaire_ids.append(resp1["id"])

    query = {"questionnaires": questionnaire_ids}

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query", json=query)
    r.raise_for_status()
    resp_query = r.json()

    assert resp_query["item_count"] == len(resp_query["items"]) == 1000


def test_query_invalid_skip_limit():
    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=7", json={})
    assert r.status_code == 412

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=7&limit=3", json={})
    assert r.status_code == 412

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=7&limit=20", json={})
    assert r.status_code == 412

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=-7&limit=20", json={})
    assert r.status_code == 422

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=-7&limit=-20", json={})
    assert r.status_code == 422

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=7&limit=-20", json={})
    assert r.status_code == 422

    r = requests.put(f"{hs_url}/v3/fhir/questionnaires/query?skip=0&limit=3", json={})
    assert r.status_code == 200
