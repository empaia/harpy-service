from uuid import uuid4

import requests

from tests.sample_data import SAMPLE_QUESTIONNAIRE_RESPONSE
from tests.singletons import hs_url


def test_case_header():
    case_id = str(uuid4())
    headers = {"case-id": case_id}
    r = requests.post(f"{hs_url}/v3/fhir/questionnaire-responses", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE)
    r.raise_for_status()
    questionnaire_response = r.json()
    logical_id = questionnaire_response["id"]

    r = requests.get(
        f"{hs_url}/v3/fhir/questionnaire-responses/{logical_id}", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE
    )
    r.raise_for_status()

    r = requests.get(f"{hs_url}/v3/fhir/questionnaire-responses/{logical_id}", json=SAMPLE_QUESTIONNAIRE_RESPONSE)
    r.raise_for_status()

    headers = {"case-id": str(uuid4())}
    r = requests.get(
        f"{hs_url}/v3/fhir/questionnaire-responses/{logical_id}", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE
    )
    error = r.json()
    assert r.status_code == 412
    assert error["detail"] == "Questionnaire response case does not match case-id HTTP header."

    r = requests.post(f"{hs_url}/v3/fhir/questionnaire-responses", json=SAMPLE_QUESTIONNAIRE_RESPONSE)
    r.raise_for_status()
    questionnaire_response = r.json()
    logical_id2 = questionnaire_response["id"]

    r = requests.get(
        f"{hs_url}/v3/fhir/questionnaire-responses/{logical_id2}", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE
    )
    error = r.json()
    assert r.status_code == 412
    assert error["detail"] == "Questionnaire response case mapping does not exist to match case-id HTTP header."


def test_cases_filter():
    case_id1 = str(uuid4())
    headers = {"case-id": case_id1}
    result_ids1 = []
    for _ in range(3):
        r = requests.post(
            f"{hs_url}/v3/fhir/questionnaire-responses", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE
        )
        r.raise_for_status()
        data = r.json()
        result_ids1.append(data["id"])

    case_id2 = str(uuid4())
    headers = {"case-id": case_id2}
    result_ids2 = []
    for _ in range(2):
        r = requests.post(
            f"{hs_url}/v3/fhir/questionnaire-responses", headers=headers, json=SAMPLE_QUESTIONNAIRE_RESPONSE
        )
        r.raise_for_status()
        data = r.json()
        result_ids2.append(data["id"])

    query = {"questionnaire_responses": result_ids1}
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids1)

    query = {"questionnaire_responses": result_ids2}
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids2)

    query = {"questionnaire_responses": result_ids1 + result_ids2}
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids1) + len(result_ids2)

    query = {
        "questionnaire_responses": result_ids1 + result_ids2,
        "cases": [case_id1, case_id2],
    }
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids1) + len(result_ids2)

    query = {
        "questionnaire_responses": result_ids1 + result_ids2,
        "cases": [case_id1],
    }
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids1)

    query = {
        "questionnaire_responses": result_ids1,
        "cases": [case_id1, case_id2],
    }
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids1)

    query = {
        "cases": [case_id2],
    }
    r = requests.put(
        f"{hs_url}/v3/fhir/questionnaire-responses/query",
        headers=headers,
        json=query,
    )
    r.raise_for_status()
    data = r.json()
    assert len(data["items"]) == data["item_count"] == len(result_ids2)
