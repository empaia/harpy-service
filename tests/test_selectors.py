import uuid

import requests

from harpy_service.models.v3.fhir.commons import FHIRResourceType
from tests.sample_data import SAMPLE_QUESTIONNAIRE

from .singletons import hs_url


def test_post_selectors_invalid_payload():
    r = requests.post(f"{hs_url}/v3/fhir/selectors", json={})
    assert r.status_code == 422

    json = {"type": "QuestionnaireResponse", "logical_id": "dummy", "selector_value": "selector1"}  # not supported

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    assert r.status_code == 422
    assert r.json()["detail"][0]["msg"] == "Input should be <FHIRResourceType.QUESTIONNAIRE: 'Questionnaire'>"


def test_post_selectors_invalid_logical_id():
    logical_id = str(uuid.uuid4())
    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": logical_id, "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    assert r.status_code == 404
    assert r.json()["detail"] == f"Referenced resource with ID {logical_id} not found"


def test_post_valid_selector():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    assert r.status_code == 201
    resp_selector = r.json()
    assert "id" in resp_selector
    assert resp_selector["type"] == json["type"]
    assert resp_selector["logical_id"] == json["logical_id"]
    assert resp_selector["selector_value"] == json["selector_value"]


def test_post_selector_unique_constraint():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    assert r.status_code == 400
    assert r.json()["detail"] == f"Selector for {selector} already exists in database"


def test_get_selector_invalid_id():
    selector_id = str(uuid.uuid4())
    r = requests.get(f"{hs_url}/v3/fhir/selectors/{selector_id}")
    assert r.status_code == 404
    assert r.json()["detail"] == f"Selector with ID {selector_id} not found"


def test_get_selector_valid_id():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/selectors/{resp['id']}")
    resp_get = r.json()
    r.raise_for_status()
    assert resp_get["type"] == json["type"]
    assert resp_get["logical_id"] == json["logical_id"]
    assert resp_get["selector_value"] == json["selector_value"]


def test_query_selectors():
    # create questionaire 1
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest1 = r.json()

    # create questionaire 2
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest2 = r.json()

    # create questionaire 3
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest3 = r.json()

    # create selector 1 - quest 1
    selector1 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest1["id"], "selector_value": selector1}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp_selector1 = r.json()

    # create selector 2 - quest 1
    selector2 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest1["id"], "selector_value": selector2}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp_selector2 = r.json()

    # create selector 3 - quest 2
    selector3 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest2["id"], "selector_value": selector3}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp_selector3 = r.json()

    # create selector 4 - quest 3
    selector4 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest3["id"], "selector_value": selector4}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp_selector4 = r.json()

    # ----
    # Query tests

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json={})
    r.raise_for_status()
    resp_base_query = r.json()
    assert resp_base_query["item_count"] == len(resp_base_query["items"])

    # invalid query
    query = {"some_other_property": "test"}

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json=query)
    assert r.status_code == 422

    # invalid query - not a list
    query = {"types": FHIRResourceType.QUESTIONNAIRE.value}

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json=query)
    assert r.status_code == 422

    # valid query - should yield the same results as base query
    query = {"types": [FHIRResourceType.QUESTIONNAIRE.value]}

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json=query)
    r.raise_for_status()
    resp_query1 = r.json()
    assert resp_query1 == resp_base_query

    # valid query - should yield all chosen selectors
    query = {
        "types": [FHIRResourceType.QUESTIONNAIRE.value],
        "selectors": [resp_selector1["id"], resp_selector2["id"], resp_selector3["id"], resp_selector4["id"]],
    }
    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json=query)
    r.raise_for_status()
    resp_query2 = r.json()
    assert resp_query2["item_count"] == len(resp_query2["items"]) == 4
    for item in resp_query2["items"]:
        assert item["id"] in query["selectors"]

    # valid query - should yield all chosen selectors
    query = {
        "types": [FHIRResourceType.QUESTIONNAIRE.value],
        "selectors": [resp_selector1["id"], resp_selector2["id"], resp_selector3["id"], resp_selector4["id"]],
        "selector_values": [selector1, selector2],
    }

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query", json=query)
    r.raise_for_status()
    resp_query3 = r.json()
    assert resp_query3["item_count"] == len(resp_query3["items"]) == 2
    for item in resp_query3["items"]:
        assert item["id"] in [resp_selector1["id"], resp_selector2["id"]]

    # valid query - skip and limit
    query = {
        "types": [FHIRResourceType.QUESTIONNAIRE.value],
        "selectors": [resp_selector1["id"], resp_selector2["id"], resp_selector3["id"], resp_selector4["id"]],
    }

    r = requests.put(f"{hs_url}/v3/fhir/selectors/query?skip=2&limit=2", json=query)
    r.raise_for_status()
    resp_query2 = r.json()
    assert resp_query2["item_count"] == 4
    assert len(resp_query2["items"]) == 2
    for item in resp_query2["items"]:
        assert item["id"] in [resp_selector3["id"], resp_selector4["id"]]


def test_delete_selector_valid_id():
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()
    resp = r.json()

    r = requests.delete(f"{hs_url}/v3/fhir/selectors/{resp['id']}")
    r.raise_for_status()

    r = requests.get(f"{hs_url}/v3/fhir/selectors/{resp['id']}")
    assert r.status_code == 404
