import uuid

import requests

from tests.sample_data import SAMPLE_QUESTIONNAIRE

from .singletons import hs_url


def test_post_selector_taggings_invalid_payload():
    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json={})
    assert r.status_code == 422

    json = {"type": "Questionnaire", "selector_value": "selector1"}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    assert r.status_code == 422

    json = {
        "type": "QuestionnaireResponse",
        "selector_value": "selector1",
        "indication": "test_ind",
        "procedure": "test_proced",
    }

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    assert r.status_code == 422
    assert r.json()["detail"][0]["msg"] == "Input should be <FHIRResourceType.QUESTIONNAIRE: 'Questionnaire'>"


def test_post_valid_selector_tagging():
    # create questionnaire
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    # create selector
    selector = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    # create tagging
    indication = f"indication.{uuid.uuid4()}"
    procedure = f"procedure.{uuid.uuid4()}"
    json = {"type": "Questionnaire", "selector_value": selector, "indication": indication, "procedure": procedure}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    r.raise_for_status()
    resp_tagging1 = r.json()
    assert "id" in resp_tagging1
    assert resp_tagging1["type"] == json["type"]
    assert resp_tagging1["selector_value"] == json["selector_value"]
    assert resp_tagging1["indication"] == json["indication"]
    assert resp_tagging1["procedure"] == json["procedure"]

    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings/{resp_tagging1['id']}")
    r.raise_for_status()
    resp_tagging2 = r.json()
    assert resp_tagging1 == resp_tagging2


def test_post_valid_selector_tagging_unique_constraint():
    # create questionnaire
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    # create selectors
    selector1 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector1}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    selector2 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector2}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    # create tagging
    indication = f"indication.{uuid.uuid4()}"
    procedure = f"procedure.{uuid.uuid4()}"
    json = {"type": "Questionnaire", "selector_value": selector1, "indication": indication, "procedure": procedure}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    r.raise_for_status()

    # must fail: same selector - different tags
    json = {
        "type": "Questionnaire",
        "selector_value": selector1,
        "indication": f"{indication}_new",
        "procedure": f"{procedure}_new",
    }

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    assert r.status_code == 412
    result_text = f"Unique key constraint violated: Selector mapping for selector {selector1} already exists"
    assert r.json()["detail"] == result_text

    # must fail: new selector - same tags
    json = {"type": "Questionnaire", "selector_value": selector2, "indication": indication, "procedure": procedure}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    assert r.status_code == 412
    result_text = "Unique key constraint violated: Type, indication and procedure must be a unique combination"
    assert r.json()["detail"] == result_text


def test_get_selector_tagging_invalid_id():
    selector_tagging_id = str(uuid.uuid4())
    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings/{selector_tagging_id}")
    assert r.status_code == 404
    assert r.json()["detail"] == f"Selector with ID {selector_tagging_id} not found"


def test_get_selector_tagging_valid_id():
    # create questionnaire
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    # create selectors
    selector1 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector1}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    # create tagings
    indication = f"indication.{uuid.uuid4()}"
    procedure = f"procedure.{uuid.uuid4()}"
    json = {"type": "Questionnaire", "selector_value": selector1, "indication": indication, "procedure": procedure}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    r.raise_for_status()
    resp_post = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings/{resp_post['id']}")
    resp_get = r.json()
    r.raise_for_status()
    assert resp_post == resp_get


def test_get_all_selector_taggings():
    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings")
    r.raise_for_status()
    resp_get1 = r.json()

    # create questionnaire
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    # create selectors
    selector1 = str(uuid.uuid4())
    json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector1}

    r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
    r.raise_for_status()

    # create tagings
    indication = f"indication.{uuid.uuid4()}"
    procedure = f"procedure.{uuid.uuid4()}"
    json = {"type": "Questionnaire", "selector_value": selector1, "indication": indication, "procedure": procedure}

    r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
    r.raise_for_status()

    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings")
    r.raise_for_status()
    resp_get2 = r.json()
    assert resp_get1["item_count"] + 1 == resp_get2["item_count"]
    assert len(resp_get1["items"]) + 1 == len(resp_get2["items"])


def test_get_all_selector_taggings_skip_limit():
    # create questionnaire
    r = requests.post(f"{hs_url}/v3/fhir/questionnaires", json=SAMPLE_QUESTIONNAIRE)
    r.raise_for_status()
    resp_quest = r.json()

    # create selectors
    for _ in range(0, 15):
        selector = str(uuid.uuid4())
        json = {"type": "Questionnaire", "logical_id": resp_quest["id"], "selector_value": selector}

        r = requests.post(f"{hs_url}/v3/fhir/selectors", json=json)
        r.raise_for_status()

        # create tagings
        indication = f"indication.{uuid.uuid4()}"
        procedure = f"procedure.{uuid.uuid4()}"
        json = {"type": "Questionnaire", "selector_value": selector, "indication": indication, "procedure": procedure}

        r = requests.post(f"{hs_url}/v3/fhir/selector-taggings", json=json)
        r.raise_for_status()

    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings")
    r.raise_for_status()
    resp_get1 = r.json()

    r = requests.get(f"{hs_url}/v3/fhir/selector-taggings?skip=5&limit=10")
    r.raise_for_status()
    resp_get2 = r.json()
    assert resp_get2["item_count"] == resp_get1["item_count"]
    assert len(resp_get2["items"]) == 10
    assert resp_get2["items"] == resp_get1["items"][5:15]
