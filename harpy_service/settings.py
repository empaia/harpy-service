from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    debug: bool = False

    db_host: str = "localhost"
    db_port: str = 5432
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "hs"

    fhir_server_url: str = ""
    fhir_query_resource_limit: int = 1000

    model_config = SettingsConfigDict(
        env_file=".env",
        env_prefix="HS_",
        extra="ignore",
    )  # change env var prefix, TS is harpy Service
