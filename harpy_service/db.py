import json

from asyncpg import Connection

from harpy_service.clients.case_client import CaseClient
from harpy_service.clients.selector_client import FHIRResourceSelectorClient


def _jsonb_dumps(value):
    return b"\x01" + json.dumps(value).encode("utf-8")


def _jsonb_loads(value: bytes):
    return json.loads(value[1:].decode("utf-8"))


async def set_type_codecs(conn: Connection):
    await conn.set_type_codec("json", encoder=json.dumps, decoder=json.loads, schema="pg_catalog", format="text")
    await conn.set_type_codec("jsonb", encoder=_jsonb_dumps, decoder=_jsonb_loads, schema="pg_catalog", format="binary")


class Clients:
    def __init__(self, conn):
        self.selector_client = FHIRResourceSelectorClient(conn=conn)
        self.case_client = CaseClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
