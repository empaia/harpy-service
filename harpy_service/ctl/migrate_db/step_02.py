#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_02(conn):

    await conn.execute(
        """
        CREATE TABLE fhir_questionnaire_response_case_mapping (
            id text PRIMARY KEY,
            case_id text NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE INDEX case_id ON fhir_questionnaire_response_case_mapping USING btree (case_id);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
