#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    await conn.execute(
        """
        CREATE TABLE fhir_resource_selectors (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            type text NOT NULL,
            logical_id text NOT NULL,
            selector_value text NOT NULL UNIQUE,
            created_at timestamp NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE fhir_resource_selector_taggings (
            id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
            type text NOT NULL,
            selector_value text NOT NULL UNIQUE,
            indication text NOT NULL,
            procedure text NOT NULL,
            created_at timestamp NOT NULL,
            UNIQUE (type, indication, procedure)
        );
        """
    )

    await conn.execute(
        """
        CREATE INDEX taggings_indication ON fhir_resource_selector_taggings USING btree (indication);
        CREATE INDEX taggings_procedure ON fhir_resource_selector_taggings USING btree (procedure);
        CREATE INDEX selector_value ON fhir_resource_selector_taggings USING btree (selector_value);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
