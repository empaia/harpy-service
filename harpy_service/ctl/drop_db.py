from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db
from .migrate_db import TABLE_NAME_PREFIX

TABLES_MISC = [
    f"{TABLE_NAME_PREFIX}_migration_steps",
]

TABLES = [
    "fhir_resource_selectors",
]

TABLES = [*TABLES_MISC, *TABLES]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table} CASCADE;")
        except UndefinedTableError as e:
            print(e)
