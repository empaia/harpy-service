from asyncpg import PostgresError, UniqueViolationError
from fastapi import HTTPException

from harpy_service.clients.selector_sql_helpers import (
    get_delete_statement,
    get_insert_selector_statement,
    get_insert_selector_tagging_statement,
    get_query_selector_statement,
    get_select_statement,
    get_select_statement_skip_limit,
)
from harpy_service.clients.utils import log_and_raise_postgres_error
from harpy_service.models.v3.fhir.selectors import (
    PostSelector,
    PostSelectorTagging,
    Selector,
    SelectorList,
    SelectorQuery,
    SelectorTagging,
    SelectorTaggingList,
)


class FHIRResourceSelectorClient:
    def __init__(self, conn):
        self.conn = conn

    async def post_selector(self, selector: PostSelector) -> Selector:
        sql_instert_statement = get_insert_selector_statement()
        values = [selector.type, selector.logical_id, selector.selector_value]

        try:
            record = await self.conn.fetchrow(sql_instert_statement, *values)
            return Selector(**record)
        except UniqueViolationError as e1:
            raise HTTPException(
                status_code=400, detail=f"Selector for {selector.selector_value} already exists in database"
            ) from e1
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector could not be created")

    async def get_selector(self, selector_id: str) -> Selector:
        sql_get_statement = get_select_statement(table_name="fhir_resource_selectors")

        try:
            record = await self.conn.fetchrow(sql_get_statement, selector_id)
            if record is None:
                raise HTTPException(status_code=404, detail=f"Selector with ID {selector_id} not found")

            return Selector(**record)
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector could not be retrieved")

    async def delete_selector(self, selector_id: str) -> Selector:
        sql_delete_statement = get_delete_statement(table_name="fhir_resource_selectors")

        try:
            record = await self.conn.fetchrow(sql_delete_statement, selector_id)
            return Selector(**record)
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector could not be deleted")

    async def query_selectors(self, query: SelectorQuery, skip: int = None, limit: int = None) -> SelectorList:
        sql_query_statement = get_query_selector_statement(
            filter_by_type=(query and query.types),
            filter_by_id=(query and query.selectors),
            filter_by_value=(query and query.selector_values),
            skip_entries=(skip is not None),
            limit_entries=(limit is not None),
        )

        args = []
        if query:
            if query.types:
                args.append(query.types)
            if query.selectors:
                args.append(query.selectors)
            if query.selector_values:
                args.append(query.selector_values)
        if limit is not None:
            args.append(limit)
        if skip is not None:
            args.append(skip)

        try:
            selector_records = await self.conn.fetch(sql_query_statement, *args)
            selector_results = []
            for record in selector_records:
                selector_results.append(Selector(**record["selector_obj"]))
            if len(selector_results) == 0:
                return SelectorList(items=[], item_count=0)
            return SelectorList(items=selector_results, item_count=selector_records[0]["full_count"])
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector could not be queried")

    async def post_selector_tagging(self, selector_tagging: PostSelectorTagging) -> SelectorTagging:
        # TODO: should we check tag validity here?

        sql_instert_statement = get_insert_selector_tagging_statement()
        values = [
            selector_tagging.type,
            selector_tagging.selector_value,
            selector_tagging.indication,
            selector_tagging.procedure,
        ]

        try:
            record = await self.conn.fetchrow(sql_instert_statement, *values)
            return SelectorTagging(**record)
        except UniqueViolationError as e1:
            msg = "Type, indication and procedure must be a unique combination"
            if "selector_value" in str(e1):
                msg = f"Selector mapping for selector {selector_tagging.selector_value} already exists"
            raise HTTPException(
                status_code=412,
                detail=f"Unique key constraint violated: {msg}",
            ) from e1
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector tagging could not be created")

    async def get_selector_tagging(self, selector_tagging_id: str) -> SelectorTagging:
        sql_get_statement = get_select_statement(table_name="fhir_resource_selector_taggings")

        try:
            record = await self.conn.fetchrow(sql_get_statement, selector_tagging_id)
            if record is None:
                raise HTTPException(status_code=404, detail=f"Selector with ID {selector_tagging_id} not found")

            return SelectorTagging(**record)
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector tagging could not be retrieved")

    async def delete_selector_tagging(self, selector_tagging_id: str) -> SelectorTagging:
        sql_delete_statement = get_delete_statement(table_name="fhir_resource_selector_taggings")

        try:
            record = await self.conn.fetchrow(sql_delete_statement, selector_tagging_id)
            return SelectorTagging(**record)
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector tagging could not be deleted")

    async def get_selector_taggings(self, skip: int = None, limit: int = None) -> SelectorTaggingList:
        sql_get_statement = get_select_statement_skip_limit(
            table_name="fhir_resource_selector_taggings",
            skip_entries=(skip is not None),
            limit_entries=(limit is not None),
        )

        args = []
        if limit is not None:
            args.append(limit)
        if skip is not None:
            args.append(skip)

        try:
            selector_mapping_records = await self.conn.fetch(sql_get_statement, *args)
            selector_mapping_results = []
            for record in selector_mapping_records:
                selector_mapping_results.append(SelectorTagging(**record["selector_mapping_obj"]))
            return SelectorTaggingList(
                item_count=selector_mapping_records[0]["full_count"], items=selector_mapping_results
            )
        except PostgresError as e2:
            log_and_raise_postgres_error(e2, "Selector tagging could not be retrieved")
