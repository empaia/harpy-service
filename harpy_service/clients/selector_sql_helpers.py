def get_insert_selector_statement() -> str:
    return """
        INSERT INTO public.fhir_resource_selectors (
            id, type, logical_id, selector_value, created_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3, CURRENT_TIMESTAMP
        )
        RETURNING * ;
    """


def get_insert_selector_tagging_statement() -> str:
    return """
        INSERT INTO public.fhir_resource_selector_taggings (
            id, type, selector_value, indication, procedure, created_at
        )
        VALUES(
            uuid_generate_v4(), $1, $2, $3, $4, CURRENT_TIMESTAMP
        )
        RETURNING * ;
    """


def get_select_statement(table_name: str) -> str:
    base_query = f"""
        SELECT *
        FROM public.{table_name}
        WHERE id = $1 ;
    """
    return base_query.format(table_name=table_name)


def get_select_statement_skip_limit(table_name: str, limit_entries: bool, skip_entries: bool) -> str:
    index = 0
    limitation = ""
    if limit_entries:
        index += 1
        limitation = f"LIMIT ${index} "
    if skip_entries:
        index += 1
        limitation = f"{limitation}OFFSET ${index}"

    base_query = """
        SELECT {selector_tagging} as selector_mapping_obj, count(*) OVER() AS full_count
        FROM public.{table_name}
        ORDER BY created_at
        {limitation} ;
    """
    return base_query.format(
        selector_tagging=_build_jsonb_selector_tagging_obj(), table_name=table_name, limitation=limitation
    )


def get_delete_statement(table_name: str) -> str:
    return f"""
        DELETE FROM public.{table_name}
        WHERE id = $1
        RETURNING * ;
    """


def get_query_selector_statement(
    filter_by_type: bool, filter_by_id: bool, filter_by_value: bool, skip_entries: bool, limit_entries: bool
) -> str:
    index = 0
    conditions = []
    if filter_by_type:
        index += 1
        conditions.append(f"type = ANY(${index})")
    if filter_by_id:
        index += 1
        conditions.append(f"id = ANY(${index})")
    if filter_by_value:
        index += 1
        conditions.append(f"selector_value = ANY(${index})")

    where_clause = ""
    if index > 0:
        where_clause = f"WHERE {' AND '.join(conditions)}"

    limitation = ""
    if limit_entries:
        index += 1
        limitation = f"LIMIT ${index} "
    if skip_entries:
        index += 1
        limitation = f"{limitation}OFFSET ${index}"

    base_sql = """
        SELECT {selector} as selector_obj, count(*) OVER() AS full_count
        FROM public.fhir_resource_selectors
        {where_clause}
        ORDER BY created_at
        {limitation} ;
    """
    return base_sql.format(selector=_build_jsonb_selector_obj(), where_clause=where_clause, limitation=limitation)


def _build_jsonb_selector_obj():
    sql = """
        jsonb_build_object(
            'id', id,
            'type', type,
            'logical_id', logical_id,
            'selector_value', selector_value,
            'created_at', EXTRACT(epoch FROM created_at)::int
        )
    """
    return sql


def _build_jsonb_selector_tagging_obj():
    sql = """
        jsonb_build_object(
            'id', id,
            'type', type,
            'selector_value', selector_value,
            'indication', indication,
            'procedure', procedure,
            'created_at', EXTRACT(epoch FROM created_at)::int
        )
    """
    return sql
