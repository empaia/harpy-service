def _json_build_object():
    return """json_build_object(
        'id', id,
        'case_id', case_id
    )"""


def get_insert_statement() -> str:
    return """
        INSERT INTO public.fhir_questionnaire_response_case_mapping (
            id, case_id
        )
        VALUES(
            $1, $2
        )
        RETURNING * ;
    """


def get_select_statement() -> str:
    return f"""
        SELECT {_json_build_object()}
        FROM public.fhir_questionnaire_response_case_mapping
        WHERE id = $1 ;
    """


def get_select_by_cases_statement() -> str:
    return """
        SELECT array_agg(id) as agg
        FROM public.fhir_questionnaire_response_case_mapping
        WHERE case_id = ANY($1) ;
    """
