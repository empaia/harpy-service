from harpy_service.clients.case_sql_helper import (
    get_insert_statement,
    get_select_by_cases_statement,
    get_select_statement,
)


class CaseClient:
    def __init__(self, conn):
        self.conn = conn

    async def post_mapping(self, questionnaire_response_id, case_id):
        sql = get_insert_statement()
        _ = await self.conn.fetchrow(sql, questionnaire_response_id, case_id)

    async def get_mapping(self, questionnaire_response_id):
        sql = get_select_statement()
        record = await self.conn.fetchrow(sql, questionnaire_response_id)
        if record is None:
            return None
        mapping = record["json_build_object"]
        return mapping

    async def get_case_mapping_filter(self, cases):
        sql = get_select_by_cases_statement()
        record = await self.conn.fetchrow(sql, cases)
        return record["agg"]
