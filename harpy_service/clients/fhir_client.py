from typing import List

from fastapi import HTTPException
from fhirpy import AsyncFHIRClient
from fhirpy.base.exceptions import MultipleResourcesFound, OperationOutcome, ResourceNotFound

from harpy_service.models.v3.fhir.commons import FHIRResourceType
from harpy_service.models.v3.fhir.questionnaire_responses import (
    PostQuestionnaireResponse,
    QuestionnaireResponse,
    QuestionnaireResponseList,
    QuestionnaireResponseQuery,
)
from harpy_service.models.v3.fhir.questionnaires import (
    PostQuestionnaire,
    Questionnaire,
    QuestionnaireList,
    QuestionnaireQuery,
)

from ..singletons import logger, settings


class FHIRClient:
    def __init__(self, fihr_server_url: str):
        self.fihr_server_url = fihr_server_url
        self.client = None

    async def initialize(self):
        self.client = AsyncFHIRClient(self.fihr_server_url)

    async def check_alive(self):
        resources = self.client.resources("Patient")
        try:
            await resources.search().limit(1).fetch()
            return True
        except Exception as e:  # pylint: disable=broad-exception-caught
            logger.error("Error connecting to HAPI server: %s", e)
            return False

    # Questionnaires

    async def post_questionnaire(self, questionnaire: PostQuestionnaire) -> Questionnaire:
        raw_questionnaire = questionnaire.model_dump(by_alias=True, exclude_unset=True, exclude_none=True, mode="json")
        return await self.persist_resource(resource_type=FHIRResourceType.QUESTIONNAIRE, raw_resource=raw_questionnaire)

    async def put_questionnaire(self, questionnaire_id: str, questionnaire: PostQuestionnaire) -> Questionnaire:
        raw_questionnaire = questionnaire.model_dump(by_alias=True, exclude_unset=True, exclude_none=True, mode="json")
        return await self.update_resource(
            resource_type=FHIRResourceType.QUESTIONNAIRE, resource_id=questionnaire_id, raw_resource=raw_questionnaire
        )

    async def get_questionnaire(self, questionnaire_id: str) -> Questionnaire:
        return await self.validate_and_get_resource_exists(
            resource_type=FHIRResourceType.QUESTIONNAIRE, resource_id=questionnaire_id
        )

    async def get_questionnaire_history(self, questionnaire_id: str) -> List[Questionnaire]:
        return await self.get_resource_history(
            resource_type=FHIRResourceType.QUESTIONNAIRE, resource_id=questionnaire_id
        )

    async def get_questionnaire_history_item(self, questionnaire_id: str, version_id: str) -> Questionnaire:
        return await self.get_resource_history_item(
            resource_type=FHIRResourceType.QUESTIONNAIRE, resource_id=questionnaire_id, version_id=version_id
        )

    async def query_questionnaires(
        self, query: QuestionnaireQuery, skip: int = None, limit: int = None
    ) -> QuestionnaireList:
        params = {}
        if query.questionnaires and len(query.questionnaires) > 0:
            params["_id"] = ",".join(query.questionnaires)
        if query.identifiers and len(query.identifiers) > 0:
            params["identifier"] = ",".join(query.identifiers)
        if query.statuses and len(query.statuses) > 0:
            params["status"] = ",".join(query.statuses)

        return await self.execute_query(
            resource_type=FHIRResourceType.QUESTIONNAIRE, extra_params=params, skip=skip, limit=limit
        )

    # Questionnaire Responses

    async def post_questionnaire_response(
        self, questionnaire_response: PostQuestionnaireResponse
    ) -> QuestionnaireResponse:
        raw_questionnaire = questionnaire_response.model_dump(
            by_alias=True, exclude_unset=True, exclude_none=True, mode="json"
        )
        return await self.persist_resource(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE, raw_resource=raw_questionnaire
        )

    async def put_questionnaire_response(
        self, questionnaire_response_id: str, questionnaire_response: PostQuestionnaireResponse
    ) -> QuestionnaireResponse:
        raw_questionnaire_response = questionnaire_response.model_dump(
            by_alias=True, exclude_unset=True, exclude_none=True, mode="json"
        )
        return await self.update_resource(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE,
            resource_id=questionnaire_response_id,
            raw_resource=raw_questionnaire_response,
        )

    async def get_questionnaire_response(self, questionnaire_response_id: str) -> QuestionnaireResponse:
        return await self.validate_and_get_resource_exists(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE, resource_id=questionnaire_response_id
        )

    async def get_questionnaire_response_history(self, questionnaire_response_id: str) -> List[QuestionnaireResponse]:
        return await self.get_resource_history(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE, resource_id=questionnaire_response_id
        )

    async def get_questionnaire_response_history_item(
        self, questionnaire_response_id: str, version_id: str
    ) -> QuestionnaireResponse:
        return await self.get_resource_history_item(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE,
            resource_id=questionnaire_response_id,
            version_id=version_id,
        )

    async def query_questionnaire_responses(
        self,
        query: QuestionnaireResponseQuery,
        skip: int = None,
        limit: int = None,
        case_mapping_filter: List[str] = None,
    ) -> QuestionnaireResponseList:
        params = {}
        questionnaire_responses_filter = None
        if query.questionnaire_responses and case_mapping_filter:
            questionnaire_responses_filter = set(query.questionnaire_responses)
            case_mapping_filter = set(case_mapping_filter)
            questionnaire_responses_filter = questionnaire_responses_filter.intersection(case_mapping_filter)
            questionnaire_responses_filter = list(questionnaire_responses_filter)
        elif query.questionnaire_responses:
            questionnaire_responses_filter = query.questionnaire_responses
        elif case_mapping_filter:
            questionnaire_responses_filter = case_mapping_filter

        if questionnaire_responses_filter:
            params["_id"] = ",".join(questionnaire_responses_filter)
        if query.identifiers:
            params["identifier"] = ",".join(query.identifiers)
        if query.statuses:
            params["status"] = ",".join(query.statuses)
        # TODO: Investiate: querying by questionnaire does not seem to work with blaze
        # needs further investigation!
        # if query.questionnaires:
        #    params["questionnaire"] = ",".join(query.questionnaires)

        return await self.execute_query(
            resource_type=FHIRResourceType.QUESTIONNAIRE_RESPONSE, extra_params=params, skip=skip, limit=limit
        )

    # Resource-agnostic stuff

    async def persist_resource(self, resource_type: FHIRResourceType, raw_resource: dict) -> dict:
        resource = self.client.resource(resource_type=resource_type.value, **raw_resource)

        try:
            await resource.create()
            return await self.get_single_resource(resource_type=resource_type, resource_id=resource["id"])
        except OperationOutcome as e1:
            raise HTTPException(
                status_code=404, detail=f"Error persisting resource {resource_type.value}: {e1.resource}"
            ) from e1
        except Exception as e2:  # pylint: disable=broad-exception-caught
            logger.error("Error persisting resource %s: %s", resource_type.value, e2)
            raise HTTPException(status_code=500, detail=f"Error persisting resource {resource_type.value}") from e2

    async def update_resource(self, resource_type: FHIRResourceType, resource_id: str, raw_resource: dict) -> dict:
        _ = await self.validate_and_get_resource_exists(resource_type=resource_type, resource_id=resource_id)

        try:
            raw_resource["id"] = resource_id
            resource = self.client.resource(resource_type=resource_type.value, **raw_resource)
            await resource.save()
            return await self.get_single_resource(resource_type=resource_type, resource_id=resource_id)
        except OperationOutcome as e2:
            raise HTTPException(status_code=422, detail=f"Validation error: {e2}") from e2
        except MultipleResourcesFound as e3:
            logger.error("Multiple resources found for ID %s: %s", resource_id, e3)
            raise HTTPException(status_code=500, detail=f"Multiple resources found for ID {resource_id}") from e3

    async def validate_and_get_resource_exists(self, resource_type: FHIRResourceType, resource_id: str):
        resource = await self.get_single_resource(resource_type=resource_type, resource_id=resource_id)

        if resource is None:
            raise HTTPException(
                status_code=404, detail=f"Resource {resource_type.value} with ID {resource_id} does not exist"
            )

        return resource

    async def get_single_resource(self, resource_type: FHIRResourceType, resource_id: str) -> dict:
        resources = self.client.resources(resource_type=resource_type)

        try:
            response = await resources.search(_id=resource_id).get()
            serialized_reponse = response.serialize()
            return serialized_reponse
        except ResourceNotFound:
            return None
        except MultipleResourcesFound as e:
            logger.error("Multiple resources found for ID %s: %s", resource_id, e)
            raise HTTPException(status_code=500, detail=f"Multiple resources found for ID {resource_id}") from e

    async def get_resource_history(self, resource_type: FHIRResourceType, resource_id: str) -> dict:
        response = await self.client.execute(path=f"{resource_type.value}/{resource_id}/_history", method="GET")
        resource_history = [history_item["resource"] for history_item in response["entry"]]
        return resource_history

    async def get_resource_history_item(
        self, resource_type: FHIRResourceType, resource_id: str, version_id: str
    ) -> dict:
        try:
            resource_history_item = await self.client.execute(
                path=f"{resource_type.value}/{resource_id}/_history/{version_id}", method="GET"
            )
            return resource_history_item
        except ResourceNotFound as e:
            raise HTTPException(
                status_code=404,
                detail=f"History item of resource {resource_type.value} with ID {resource_id}\\{version_id} not found",
            ) from e

    async def execute_query(self, resource_type: FHIRResourceType, extra_params: dict, skip: int, limit: int) -> dict:
        params = self.validate_skip_limit_params(skip=skip, limit=limit)
        url = self.assemble_resource_url(resource_type, extra_params, params)

        resources = await self.client.execute(path="", method="POST", data=self.create_transaction_bundle(url))
        resource = resources["entry"][0]["resource"]

        if "total" not in resource:
            logger.error("Could not query resource: [Url: %s | Response: %s]", url, resources)
            raise HTTPException(status_code=400, detail="Failed to query resources")

        result = {"item_count": 0, "items": []}
        count = resource["total"]
        if count > 0 and "entry" in resource:
            result["items"] = [entry["resource"] for entry in resource["entry"]]
            result["item_count"] = count

        return result

    async def execute_query_restricted_url(
        self, resource_type: FHIRResourceType, extra_params: dict, skip: int, limit: int
    ) -> dict:
        params = self.validate_skip_limit_params(skip=skip, limit=limit)
        params.update(extra_params)

        resources = await self.client.execute(path=f"/{resource_type.value}", method="GET", params=params)
        count = resources["total"]

        result = {"item_count": count, "items": []}
        if count > 0 and "entry" in resources:
            result["items"] = [entry["resource"] for entry in resources["entry"]]

        return result

    # Helper functions

    def validate_skip_limit_params(self, skip: int, limit: int) -> dict:
        limit = limit if limit else settings.fhir_query_resource_limit

        if skip is None:
            return {"_count": limit, "_page": 0}

        if skip % limit != 0:
            raise HTTPException(
                status_code=412, detail=f"Skip parameter must be a multiple of limit (skip={skip}, limit={limit})"
            )

        page = skip / limit if skip > 0 else 0
        return {"_count": limit, "_page": page}

    def assemble_resource_url(self, resource_type: FHIRResourceType, extra_params: dict, params: dict) -> str:
        url = f"{resource_type.value}?"
        for param_key in extra_params.keys():
            url = f"{url}{param_key}={extra_params[param_key]}&"

        url = f"{url}_count={params['_count']}"

        if "_page" in params:
            url = f"{url}&_page={params['_page']}"

        # TODO: as soon as sorted filter option is set, the returned search set does not contain the
        # total count of the search result; for now we use the default sort order of the fhir server
        #  url = f"{url}&_sort=-_lastUpdated"
        return url

    def create_transaction_bundle(self, url: str) -> dict:
        entries = [{"request": {"method": "GET", "url": url}}]
        return {"resourceType": "Bundle", "type": "batch", "entry": entries}
