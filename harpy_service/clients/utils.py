from asyncpg import PostgresError
from fastapi import HTTPException

from harpy_service.singletons import logger


def log_and_raise_postgres_error(e: PostgresError, detail_message: str):
    logger.error(e)
    raise HTTPException(status_code=500, detail=f"{detail_message}. See logs for more details.")


def log_and_raise_error(e: Exception, status_code: int, detail_message: str):
    logger.error(e)
    raise HTTPException(status_code=status_code, detail=f"{detail_message}. See logs for more details.")
