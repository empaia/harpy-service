from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from harpy_service.clients.fhir_client import FHIRClient

from . import __version__ as version
from .api.v3 import add_routes_v3
from .late_init import LateInit
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""


late_init = LateInit()

fhir_client = FHIRClient(settings.fhir_server_url)


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    # initialize asyncpg
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )

    # initialize fhir client
    await fhir_client.initialize()
    yield


app = FastAPI(
    title="harpy Service",
    version=version,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
    lifespan=lifespan,
)


if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.get("/alive", tags=["Private"])
async def _():
    alive = await fhir_client.check_alive()
    if alive:
        return {"status": "ok", "version": version}
    return {"status": "err", "version": version, "detail": "Could not connect to HAPI server"}


app_v3 = FastAPI(openapi_url=openapi_url)
add_routes_v3(app=app_v3, fhir_client=fhir_client, late_init=late_init)
app.mount(path="/v3/fhir", app=app_v3)
