from typing import Annotated, List

from fastapi import FastAPI
from pydantic import Field

from harpy_service.clients.fhir_client import FHIRClient
from harpy_service.models.v3.fhir.questionnaires import (
    PostQuestionnaire,
    Questionnaire,
    QuestionnaireList,
    QuestionnaireQuery,
)


def add_routes_questionnaires(app: FastAPI, fhir_client: FHIRClient):
    @app.post(
        "/questionnaires",
        response_model=Questionnaire,
        status_code=201,
        summary="Create a questionnaire",
        tags=["Questionnaires"],
    )
    async def _(questionnaire: PostQuestionnaire) -> Questionnaire:
        return await fhir_client.post_questionnaire(questionnaire=questionnaire)

    @app.put(
        "/questionnaires/query",
        response_model=QuestionnaireList,
        summary="Query all questionnaires",
        tags=["Questionnaires"],
    )
    async def _(
        query: QuestionnaireQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
    ) -> QuestionnaireList:
        return await fhir_client.query_questionnaires(query=query, skip=skip, limit=limit)

    @app.put(
        "/questionnaires/{logical_id}",
        response_model=Questionnaire,
        summary="Create a questionnaire",
        tags=["Questionnaires"],
    )
    async def _(logical_id: str, questionnaire: PostQuestionnaire) -> Questionnaire:
        return await fhir_client.put_questionnaire(questionnaire_id=logical_id, questionnaire=questionnaire)

    @app.get(
        "/questionnaires/{logical_id}",
        response_model=Questionnaire,
        summary="Get a questionnaire item by logical ID",
        tags=["Questionnaires"],
    )
    async def _(
        logical_id: str,
    ) -> Questionnaire:
        return await fhir_client.get_questionnaire(questionnaire_id=logical_id)

    @app.get(
        "/questionnaires/{logical_id}/history",
        response_model=List[Questionnaire],
        summary="Retrieve all history items of a questionnaire",
        tags=["Questionnaires"],
    )
    async def _(
        logical_id: str,
    ) -> List[Questionnaire]:
        return await fhir_client.get_questionnaire_history(questionnaire_id=logical_id)

    @app.get(
        "/questionnaires/{logical_id}/history/{version_id}",
        response_model=Questionnaire,
        summary="Get certain questionnaire history item by ID and version",
        tags=["Questionnaires"],
    )
    async def _(
        logical_id: str,
        version_id: str,
    ) -> Questionnaire:
        return await fhir_client.get_questionnaire_history_item(questionnaire_id=logical_id, version_id=version_id)
