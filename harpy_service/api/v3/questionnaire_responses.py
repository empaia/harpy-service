from typing import Annotated, List

from fastapi import FastAPI, Header, HTTPException
from pydantic import UUID4, Field

from harpy_service.clients.fhir_client import FHIRClient
from harpy_service.db import db_clients
from harpy_service.late_init import LateInit
from harpy_service.models.v3.fhir.questionnaire_responses import (
    PostQuestionnaireResponse,
    QuestionnaireResponse,
    QuestionnaireResponseList,
    QuestionnaireResponseQuery,
)


async def _validate_case_mapping(late_init, case_id, logical_id):
    if not case_id:
        return

    case_id = str(case_id)
    async with late_init.pool.acquire() as conn:
        client = await db_clients(conn=conn)
        mapping = await client.case_client.get_mapping(questionnaire_response_id=logical_id)
        if not mapping:
            raise HTTPException(
                status_code=412,
                detail="Questionnaire response case mapping does not exist to match case-id HTTP header.",
            )
        if mapping["case_id"] != case_id:
            raise HTTPException(
                status_code=412, detail="Questionnaire response case does not match case-id HTTP header."
            )


def add_routes_questionnaire_responses(app: FastAPI, fhir_client: FHIRClient, late_init: LateInit):
    @app.post(
        "/questionnaire-responses",
        response_model=QuestionnaireResponse,
        status_code=201,
        summary="Create a new questionnaire response",
        tags=["Questionnaire Responses"],
    )
    async def _(
        questionnaire_response: PostQuestionnaireResponse,
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> QuestionnaireResponse:
        result = await fhir_client.post_questionnaire_response(questionnaire_response=questionnaire_response)
        if case_id:
            case_id = str(case_id)
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                questionnaire_response_id = result["id"]
                await client.case_client.post_mapping(
                    questionnaire_response_id=questionnaire_response_id, case_id=case_id
                )
        return result

    @app.put(
        "/questionnaire-responses/query",
        response_model=QuestionnaireResponseList,
        summary="Query questionnaire responses",
        tags=["Questionnaire Responses"],
    )
    async def _(
        query: QuestionnaireResponseQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
    ) -> QuestionnaireResponseList:
        if query.cases is not None:
            async with late_init.pool.acquire() as conn:
                client = await db_clients(conn=conn)
                case_mapping_filter = await client.case_client.get_case_mapping_filter(cases=query.cases)
                if not case_mapping_filter:
                    return QuestionnaireResponseList(item_count=0, items=[])
                return await fhir_client.query_questionnaire_responses(
                    query=query, skip=skip, limit=limit, case_mapping_filter=case_mapping_filter
                )
        return await fhir_client.query_questionnaire_responses(query=query, skip=skip, limit=limit)

    @app.put(
        "/questionnaire-responses/{logical_id}",
        response_model=QuestionnaireResponse,
        summary="Get a questionnaire response by ID",
        tags=["Questionnaire Responses"],
    )
    async def _(
        logical_id: str,
        questionnaire_response: PostQuestionnaireResponse,
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> QuestionnaireResponse:
        await _validate_case_mapping(late_init=late_init, case_id=case_id, logical_id=logical_id)
        return await fhir_client.put_questionnaire_response(
            questionnaire_response_id=logical_id,
            questionnaire_response=questionnaire_response,
        )

    @app.get(
        "/questionnaire-responses/{logical_id}",
        response_model=QuestionnaireResponse,
        summary="Get a questionnaire response by ID",
        tags=["Questionnaire Responses"],
    )
    async def _(
        logical_id: str,
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> QuestionnaireResponse:
        await _validate_case_mapping(late_init=late_init, case_id=case_id, logical_id=logical_id)
        return await fhir_client.get_questionnaire_response(questionnaire_response_id=logical_id)

    @app.get(
        "/questionnaire-responses/{logical_id}/history",
        response_model=List[QuestionnaireResponse],
        summary="Get all history items of a questionnaire response",
        tags=["Questionnaire Responses"],
    )
    async def _(
        logical_id: str,
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> List[QuestionnaireResponse]:
        await _validate_case_mapping(late_init=late_init, case_id=case_id, logical_id=logical_id)
        return await fhir_client.get_questionnaire_response_history(questionnaire_response_id=logical_id)

    @app.get(
        "/questionnaire-responses/{logical_id}/history/{version_id}",
        response_model=QuestionnaireResponse,
        summary="Get a certain questionnaire response history item by ID and version",
        tags=["Questionnaire Responses"],
    )
    async def _(
        logical_id: str,
        version_id: str,
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> QuestionnaireResponse:
        await _validate_case_mapping(late_init=late_init, case_id=case_id, logical_id=logical_id)
        return await fhir_client.get_questionnaire_response_history_item(
            questionnaire_response_id=logical_id, version_id=version_id
        )
