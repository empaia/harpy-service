from fastapi import FastAPI

from harpy_service.api.v3.selectors import add_routes_selectors
from harpy_service.clients.fhir_client import FHIRClient
from harpy_service.late_init import LateInit

from .questionnaire_responses import add_routes_questionnaire_responses
from .questionnaires import add_routes_questionnaires


def add_routes_v3(app: FastAPI, fhir_client: FHIRClient, late_init: LateInit):
    add_routes_questionnaires(app=app, fhir_client=fhir_client)
    add_routes_questionnaire_responses(app=app, fhir_client=fhir_client, late_init=late_init)
    add_routes_selectors(app=app, fhir_client=fhir_client, late_init=late_init)
