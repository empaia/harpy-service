from typing import Annotated

from fastapi import FastAPI, HTTPException
from pydantic import Field

from harpy_service.clients.fhir_client import FHIRClient
from harpy_service.db import db_clients
from harpy_service.late_init import LateInit
from harpy_service.models.v3.fhir.selectors import (
    PostSelector,
    PostSelectorTagging,
    Selector,
    SelectorList,
    SelectorQuery,
    SelectorTagging,
    SelectorTaggingList,
)


def add_routes_selectors(app: FastAPI, fhir_client: FHIRClient, late_init: LateInit):
    @app.post(
        "/selectors",
        response_model=Selector,
        status_code=201,
        summary="Create a new selector item",
        tags=["Selectors"],
    )
    async def _(selector: PostSelector) -> Selector:
        resource = await fhir_client.get_single_resource(selector.type, selector.logical_id)

        if resource is None:
            raise HTTPException(status_code=404, detail=f"Referenced resource with ID {selector.logical_id} not found")

        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.post_selector(selector=selector)

    @app.put(
        "/selectors/query",
        response_model=SelectorList,
        summary="Query selectors",
        tags=["Selectors"],
    )
    async def _(
        query: SelectorQuery,
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
    ) -> SelectorList:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.query_selectors(query=query, skip=skip, limit=limit)

    @app.get(
        "/selectors/{selector_id}",
        response_model=Selector,
        summary="Get selector item by ID",
        tags=["Selectors"],
    )
    async def _(
        selector_id: str,
    ) -> Selector:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.get_selector(selector_id=selector_id)

    @app.delete(
        "/selectors/{selector_id}",
        summary="Delete a selector item by ID",
        response_model=Selector,
        tags=["Selectors"],
    )
    async def _(
        selector_id: str,
    ) -> Selector:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.delete_selector(selector_id=selector_id)

    @app.post(
        "/selector-taggings",
        response_model=SelectorTagging,
        status_code=201,
        summary="Create a new selector tagging item",
        tags=["Selector Taggings"],
    )
    async def _(selector_tagging: PostSelectorTagging) -> SelectorTagging:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            async with client.selector_client.conn.transaction():
                selectors = await client.selector_client.query_selectors(
                    query=SelectorQuery.model_validate({"selector_values": [selector_tagging.selector_value]})
                )
                if selectors.item_count == 0:
                    raise HTTPException(
                        status_code=404, detail=f"No selector found with value {selector_tagging.selector_value}"
                    )

                return await client.selector_client.post_selector_tagging(selector_tagging=selector_tagging)

    @app.get(
        "/selector-taggings",
        response_model=SelectorTaggingList,
        summary="Get all selector tagging items",
        tags=["Selector Taggings"],
    )
    async def _(
        skip: Annotated[int, Field(ge=0)] = None,
        limit: Annotated[int, Field(ge=0)] = None,
    ) -> SelectorTaggingList:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.get_selector_taggings(skip=skip, limit=limit)

    @app.get(
        "/selector-taggings/{selector_tagging_id}",
        response_model=SelectorTagging,
        summary="Get selector tagging item by ID",
        tags=["Selector Taggings"],
    )
    async def _(selector_tagging_id: str) -> SelectorTagging:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.get_selector_tagging(selector_tagging_id=selector_tagging_id)

    @app.delete(
        "/selector-taggings/{selector_tagging_id}",
        summary="Delete a selector tagging item by ID",
        response_model=SelectorTagging,
        tags=["Selector Taggings"],
    )
    async def _(
        selector_tagging_id: str,
    ) -> SelectorTagging:
        async with late_init.pool.acquire() as conn:
            client = await db_clients(conn=conn)
            return await client.selector_client.delete_selector_tagging(selector_tagging_id=selector_tagging_id)
