# CHANGELOG

# 0.1.14

* Updated dependencies

# 0.1.13

* Updated dependencies

# 0.1.12

* Updated dependencies

# 0.1.11

* Updated dependencies
* replace docker-compose with docker compose
* upgraded Python to ^3.12

# 0.1.10

* Updated dependencies

# 0.1.9

* Updated dependencies

# 0.1.8

* Updated dependencies

# 0.1.7

* Updated dependencies

# 0.1.6

* Updated dependencies

# 0.1.5

* Updated dependencies

# 0.1.4

* Updated dependencies

# 0.1.3

* Updated dependencies

## 0.1.1 & 0.1.2

* Pin Blaze version and update Models submodule

## 0.1.0

* Added case-id header support for questionnaire responses

## 0.0.17

* Updated models submodule

## 0.0.16

* Updated models submodule

## 0.0.15

* Updated dependencies

## 0.0.14

* Set postgres version to 14

## 0.0.13

* Updated dependencies

## 0.0.12

* Updated models repo and fixed test

## 0.0.11

* Updated dependencies

## 0.0.10

* Updated dependencies

## 0.0.9

* Updated models repo (made resource id optional)
* Validate non existant resource

## 0.0.8

* Adapted fhir models (removed resourceType for most)

## 0.0.7

* Moved models to EMPAIA models repository and added submodule

## 0.0.6

* Implemented API routes, db connection
* Replaced HAPI by Blaze

## 0.0.5

* Updated dependencies

## 0.0.4

* Updated dependencies

## 0.0.3

* Updated dependencies

## 0.0.2

* Updated dependencies

## 0.0.1

* Initialized repository
